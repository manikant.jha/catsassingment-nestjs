import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';

import { AppController } from './app.controller';
import { AppService } from './app.service';
import { CatsModule } from './cats/cats.module';

@Module({
  imports: [
    CatsModule,
    MongooseModule.forRoot(
      'mongodb+srv://vkohli18:WmIsKC0SWEpSbbQN@cluster0.thy8d.mongodb.net/catsDB?authSource=admin&replicaSet=atlas-qwkoxw-shard-0&w=majority&readPreference=primary&appname=MongoDB%20Compass&retryWrites=true&ssl=true'
    )
  ],
  controllers: [AppController],
  providers: [AppService]
})
export class AppModule {}
