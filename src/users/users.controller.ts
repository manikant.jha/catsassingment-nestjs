import {
  Controller,
  Post,
  Body,
  Get,
  Param,
  Put,
  Delete,
  Query
} from '@nestjs/common';

import { UsersService } from './users.service';

@Controller('users')
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  @Post('register')
  async registerUser(
    @Body('name') name: string,
    @Body('email') email: string,
    @Body('password') password: string
  ) {
    const generatedId = await this.usersService.registerUser(
      name,
      email,
      password
    );
    return { id: generatedId };
  }
}
