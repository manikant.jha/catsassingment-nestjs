import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';

import { User } from './user.model';

@Injectable()
export class UsersService {
  constructor(@InjectModel('User') private readonly userModel: Model<User>) {}

  async registerUser(name: string, email: string, password: string) {
    const newUser = new this.userModel({
      name,
      email,
      password
    });
    const result = await newUser.save();
    return result.id as string;
  }
  
  private async findUser(id: string): Promise<User> {
    let user;
    try {
      user = await this.userModel.findById(id).exec();
    } catch (error) {
      throw new NotFoundException('Could not find User.');
    }
    if (!user) {
      throw new NotFoundException('Could not find User.');
    }
    return user;
  }
}
