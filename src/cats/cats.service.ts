import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';

import { Cat } from './cat.model';

@Injectable()
export class CatsService {
  constructor(@InjectModel('Cat') private readonly catModel: Model<Cat>) {}

  async insertCat(name: string, breed: string, age: number) {
    const newCat = new this.catModel({
      name,
      breed: breed,
      age
    });
    const result = await newCat.save();
    return result.id as string;
  }

  async getCats() {
    const cats = await this.catModel.find().exec();
    return cats.map(cat => ({
      id: cat.id,
      name: cat.name,
      breed: cat.breed,
      age: cat.age
    }));
  }

  async getCat(catId: string) {
    const cat = await this.findCat(catId);
    return {
      id: cat.id,
      name: cat.name,
      breed: cat.breed,
      age: cat.age
    };
  }

  async getCatBetweenAges(age_lte: number, age_gte: number) {
    let cats;
    try {
      cats = await this.catModel.find({
        age: { $lte: age_lte, $gte: age_gte }
      });

      cats = cats.map(cat => ({
        id: cat._id,
        age: cat.age,
        name: cat.name,
        breed: cat.breed
      }));
    } catch (error) {
      throw new NotFoundException(
        'Could not find cat in given age range.' + error
      );
    }

    if (!cats || cats.length === 0) {
      throw new NotFoundException('Could not find cat in given age range.');
    }

    return cats;
  }

  async updateCat(catId: string, name: string, breed: string, age: number) {
    const updatedCat = await this.findCat(catId);
    if (name) {
      updatedCat.name = name;
    }
    if (breed) {
      updatedCat.breed = breed;
    }
    if (age) {
      updatedCat.age = age;
    }
    updatedCat.save();
  }

  async deleteCat(catId: string) {
    const result = await this.catModel.deleteOne({ _id: catId }).exec();
    if (result.n === 0) {
      throw new NotFoundException('Could not find Cat.');
    }
  }

  private async findCat(id: string): Promise<Cat> {
    let cat;
    try {
      cat = await this.catModel.findById(id).exec();
    } catch (error) {
      throw new NotFoundException('Could not find Cat.');
    }
    if (!cat) {
      throw new NotFoundException('Could not find Cat.');
    }
    return cat;
  }
}
