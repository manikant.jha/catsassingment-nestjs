import {
  Controller,
  Post,
  Body,
  Get,
  Param,
  Put,
  Delete,
  Query
} from '@nestjs/common';

import { CatsService } from './cats.service';

@Controller('cats')
export class CatsController {
  constructor(private readonly catsService: CatsService) {}

  @Post()
  async addCat(
    @Body('name') catName: string,
    @Body('breed') catBreed: string,
    @Body('age') catAge: number
  ) {
    const generatedId = await this.catsService.insertCat(
      catName,
      catBreed,
      catAge
    );
    return { id: generatedId };
  }

  @Get()
  async getAllCats() {
    const cats = await this.catsService.getCats();
    return cats;
  }

  @Get('/search')
  getCatBetweeAge(
    @Query('age_lte') age_lte: number,
    @Query('age_gte') age_gte: number
  ) {
    return this.catsService.getCatBetweenAges(+age_lte, +age_gte);
  }

  @Get(':id')
  getCat(@Param('id') catId: string) {
    return this.catsService.getCat(catId);
  }

  @Put(':id')
  async updateCat(
    @Param('id') catId: string,
    @Body('name') catName: string,
    @Body('breed') catBreed: string,
    @Body('age') catAge: number
  ) {
    await this.catsService.updateCat(catId, catName, catBreed, catAge);
    return {
      message: 'Cat Udated Sucessfully'
    };
  }

  @Delete(':id')
  async removeCat(@Param('id') catId: string) {
    await this.catsService.deleteCat(catId);
    return {
      message: 'Cat Deleted Sucessfully'
    };
  }
}
